d = {}
allgames = []
number_of_games = int(input())
for i in range(number_of_games):
    game = input().split(';')
    allgames.append(game)
for i in range(len(allgames)):
    for k in range(len(allgames[i])):
        if k % 2 == 0:
            if not allgames[i][k] in d:
                d[allgames[i][k]] = [0, 0, 0, 0, 0]
            d[allgames[i][k]][0] += 1
for i in range(len(allgames)):
        if allgames[i][1] > allgames[i][3]:
            d[allgames[i][0]][1] += 1
            d[allgames[i][2]][3] += 1
            d[allgames[i][0]][4] += 3
        elif allgames[i][1] < allgames[i][3]:
            d[allgames[i][0]][3] += 1
            d[allgames[i][2]][1] += 1
            d[allgames[i][2]][4] += 3
        elif allgames[i][1] == allgames[i][3]:
            d[allgames[i][0]][2] += 1
            d[allgames[i][2]][2] += 1
            d[allgames[i][0]][4] += 1
            d[allgames[i][2]][4] += 1
for key, value in d.items():
    print(key, ':', sep='', end='')
    for i in range(len(value)):
        print(value[i], ' ', end='')
    print()
