def add_exception(nm, child, parent):
    nm.update({child: parent})
    for i in parent:
        if i not in nm and i != 'None':
            nm.update({i: ['None']})


def found_parent(nm, elmnt , exeception_error):
    res = 0
    if type(elmnt) == str:
        elmnt = elmnt.split()
    for i in elmnt:
        if nm[i] != ['None']:
            for k in nm.get(i):
                if k in exeception_error:
                    res = 1
                    return res
                else:
                    if k != 'None':
                        elmnt = k
                        res = found_parent(nm, elmnt, exeception_error)
                        if res == 1:
                            return res


nm = {}
res = 0
for i in range(int(input())):
    class_name = input().split(' : ')
    if len(class_name) != 1:
        add_exception(nm, class_name[0], class_name[1].split(' '))
    else:
        add_exception(nm, class_name[0], ['None'])
exception_error = []
answer = []
for i in range(int(input())):
    exception_error += input().split()
for i in range(len(exception_error)):
    varc = exception_error.pop()
    if varc not in nm:
        continue
    elif found_parent(nm, varc , exception_error) == 1:
        answer += varc.split()
for i in reversed(answer):
    print(i)


