class NonPositiveError(Exception):
    pass
class PositiveList(list):
    def append(self, value):
        if value > 0:
            super(PositiveList,self).append(value)
        else:
            raise NonPositiveError()
n = PositiveList()
n.append(3)
n.append(-3)
n.append(4)
n.append(0)
print(n)