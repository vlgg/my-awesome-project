import sys
import re

matchs = r"cat"

for line in sys.stdin:
    line = line.rstrip()
    result_line = re.findall(matchs, line)
    if len(result_line) >= 2:
        print(line)