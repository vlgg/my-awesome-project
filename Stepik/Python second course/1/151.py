class MoneyBox:
    def __init__(self, capacity=0):
        self.capacity = capacity
    def can_add(self, v):
        if self.capacity >= v:
            return True
        else:
            return False
    def add(self, v):
        self.capacity -= v
x = MoneyBox(5)
print(x.can_add(10))
x.add(5)
print(x.capacity)