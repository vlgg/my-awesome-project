n, k = map(int, input().split())
def rec_func (n, k):
    if k == 0:
        return 1
    elif k > n:
        return 0
    return rec_func(n-1, k) + rec_func(n-1, k-1)
print(rec_func(n, k))