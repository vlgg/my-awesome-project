def create_namespace(nm, nm_add, nm_parent):
    nm.update({nm_add:{'parent':nm_parent, 'vars':[]}})
def add_var(nm, nm_parent, var):
    nm[nm_parent]['vars'].append(var)
def get_var(nm, nm_parent, var):
    if var in nm[nm_parent]['vars']:
        print(nm_parent)
    else:
        if nm[nm_parent].get('parent') != 'None':
            nm_parent = nm[nm_parent].get('parent')
            get_var(nm, nm_parent, var)
        else:
            print('None')
nm = {'global':{'parent':'None', 'vars':[]}}
for i in range(int(input())):
    command = input().split(' ')
    if command[0] == 'create':
        create_namespace(nm, command[1], command[2])
    elif command[0] == 'add':
        add_var(nm, command[1], command[2])
    elif command[0] == 'get':
        get_var(nm, command[1], command[2])