def update_dict(nm, key, parent_name):
    nm.update({key:parent_name})
    for i in parent_name:
        if i not in nm:
            nm.update({i:'None'})
def is_parent(nm, class_parent,class_child):
    res = 0
    if type(class_child) == str:
        class_child = class_child.split()
    for i in class_child:
        if i == 'None':
                res = 0
        elif class_parent in nm[i]:
                res = 1
                break
        else:
            if nm[i] != 'None':
                class_child = nm[i]
                res = is_parent(nm, class_parent, class_child)
                if res == 1:
                    break
            else:
                res = 0
    return res
nm = {}
for i in range(int(input())):
    class_name = input().split(' : ')
    if len(class_name) != 1:
        update_dict(nm, class_name[0], class_name[1].split(' '))
    else:
        update_dict(nm, class_name[0], ['None'])
for i in range(int(input())):
    classes_name = input().split(' ')
    if classes_name[0] not in nm or classes_name[1] not in nm:
        print('No')
    elif classes_name[0] != classes_name[1]:
        n = is_parent(nm, classes_name[0],classes_name[1])
        if n == 0:
            print('No')
        else:
            print('Yes')
    else:
        print('Yes')