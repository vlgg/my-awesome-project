import time

class Loggable:
    def log(self, msg):
        print(str(time.ctime()) + ": " + str(msg))
class LoggableList(list, Loggable):
    def append(self, msg):
        self.log(msg)
        super(LoggableList, self).append(msg)

ml = LoggableList([5,4,3,2,1])
ml.append(5)
print(ml)