class Buffer:
    def __init__(self):
        self.buf = []
    def add(self, *a):
        self.buf += a
        if len(self.buf) // 5 > 0:
            for k in range(len(self.buf) // 5):
                sum = 0
                for i in range(5):
                    sum += self.buf[i]
                print(sum)
                for i in range(5):
                    del self.buf[0]
    def get_current_part(self):
        return self.buf
buf = Buffer()
buf.add(1,2,3)
buf.get_current_part()
buf.add(4, 5, 6)
buf.get_current_part()
buf.add(7, 8, 9, 10)
buf.get_current_part()
buf.add(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
buf.get_current_part()