import sys
import re

matchs = r"\b[aA]+\b"

for line in sys.stdin:
    line = line.rstrip()
    result_line = re.sub(matchs, "argh", line, count=1)
    print(result_line)
