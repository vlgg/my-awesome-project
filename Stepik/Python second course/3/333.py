import sys
import re

matchs = r"z.{3}z"

for line in sys.stdin:
    line = line.rstrip()
    result_line = re.findall(matchs, line)
    if len(result_line) >= 1:
        print(line)
