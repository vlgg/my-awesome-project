import requests

token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJyb2xlcyI6IiIsInN1YmplY3RfYXBwbGljYXRpb24iOiI1ZTM5ZjNlNDg5ZjgxZTAwMTIxNjlkODgiLCJleHAiOjE1ODE0NjEwOTIsImlhdCI6MTU4MDg1NjI5MiwiYXVkIjoiNWUzOWYzZTQ4OWY4MWUwMDEyMTY5ZDg4IiwiaXNzIjoiR3Jhdml0eSIsImp0aSI6IjVlMzlmM2U0MDFmMjdhMDAwZTM1ZDY4MCJ9.Z3NqY-LRIxq7du5nAW8GbvnBhZ6L1OLv6O_ds-u8hNk"
headers = {"X-Xapp-Token" : token}
d = {}

with open(r"C:\Users\Vlg\Downloads\dataset_24476_4 (1).txt", "r") as f:
    for line in f:
        url = "https://api.artsy.net/api/artists/" + line.strip()
        res = requests.get(url, headers=headers)
        data = res.json()
        if data["birthday"] not in d:
            d.update({data["birthday"]: [data["sortable_name"]]})
        else:
            d.update({data["birthday"]: d[data["birthday"]] + [data["sortable_name"]]})
pre_result = [*d]
pre_result.sort()
for i in pre_result:
    d[i].sort()
    for k in d[i]:
        print(k)
