import json

dict = {}
result = []
json_data = json.loads(input())
def parent_func(k,child):
    for i in json_data:
        if i["name"] == k and i["parents"] != []:
            for l in i["parents"]:
                if child not in dict[l]:
                    dict.update({l: dict[l] + [child]})
                parent_func(l, child)

for i in json_data:
    if i["name"] not in dict:
        dict.update({i["name"]:[i["name"]]})
    for k in i["parents"]:
        dict.update({k:[k]})
for i in json_data:
    for k in i["parents"]:
        if i["name"] not in dict[k]:
            dict.update({k: dict[k] + [i["name"]]})
        parent_func(k, i["name"])

for i in dict:
    result.append((i +" : " + str(len(dict[i]))))
result.sort()
for i in result:
    print(i)
