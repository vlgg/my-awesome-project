import sys
import re

matchs = r"\b(\w)(\w)"

for line in sys.stdin:
    line = line.rstrip()
    result_line = re.sub(matchs, r"\2\1", line)
    print(result_line)
