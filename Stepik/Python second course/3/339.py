import sys
import re

matchs = r"(\w)\1+"

for line in sys.stdin:
    line = line.rstrip()
    result_line = re.sub(matchs, r"\1", line)
    print(result_line)
