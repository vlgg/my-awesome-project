from lxml import etree as ET

data = ET.fromstring(input())
cube_values = {"red": 0, "green": 0, "blue": 0}


def color(color_attrib, number):
    if color_attrib['color'] == 'red':
        cube_values['red'] = cube_values['red'] + number
    elif color_attrib['color'] == 'green':
        cube_values['green'] = cube_values['green'] + number
    elif color_attrib['color'] == 'blue':
        cube_values['blue'] = cube_values['blue'] + number


def recursion(data, number):
    for element in data:
        color(element.attrib, number)
        recursion(element, number + 1)


number = 1
color(data.attrib, number)
recursion(data, number + 1)
print("%s %s %s" % (cube_values['red'], cube_values['green'], cube_values['blue']))
