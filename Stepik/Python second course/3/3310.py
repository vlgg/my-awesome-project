import sys
import re

matchs = r"(([01])([01])|([0,1]))"
nmatchs = r".[^01]"
one = 0
two = 0

for line in sys.stdin:
    line = line.rstrip()
    result = re.findall(matchs, line)
    if len(line) != 1 and re.match(nmatchs, line) == None:
        for list_numbers in result:
            if len(list_numbers[0]) == 2:
                one += int(list_numbers[1])
                two += int(list_numbers[2])
            elif len(list_numbers[0]) == 1:
                one += int(list_numbers[3])
        if (one - two) % 3 == 0:
            print(line)
    elif len(line) == 1 and line == "0":
        print(line)
    one = 0
    two = 0
