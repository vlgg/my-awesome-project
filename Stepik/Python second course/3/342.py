import requests
import re

result = []
response = requests.get(str(input()).strip())
pre_result = re.findall("(?:<a .*href=[\w\d'\"]*://)(\w[\.\w\d-]*)(?:[/:'\"].*)", response.text)
for i in pre_result:
    if i not in result:
        result.append(i)
result.sort()
for i in result:
    if len(i)>0:
        print(i)