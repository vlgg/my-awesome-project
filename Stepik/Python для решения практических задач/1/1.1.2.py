from urllib.request import urlopen
import re

html = urlopen('https://stepik.org/media/attachments/lesson/209719/2.html').read().decode('utf-8')
string_html = str(html)
d = {}
bigger_str= ''
answer = []
for i in re.findall("<code>(.*?)</code>", string_html):
    if not d.get(i):
        d[i] = 1
    else:
        d[i] = d[i] + 1
mx = d[max(d, key=d.get)]
for key, value in d.items():
    if value == mx:
        print(key, end=' ')


