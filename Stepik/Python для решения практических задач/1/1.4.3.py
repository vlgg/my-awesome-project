from urllib.request import urlopen
from bs4 import BeautifulSoup

html = urlopen('https://stepik.org/media/attachments/lesson/209723/5.html').read().decode('utf-8')
soup = BeautifulSoup(html, 'lxml')
table = soup.find_all('table')[0]
sm = 0
for row in table.find_all('tr'):
        columns = row.find_all('td')
        for column in columns:
            sm += float(column.get_text())
print(sm)