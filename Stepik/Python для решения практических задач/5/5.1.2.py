size = 10
table = []
for i in range(size):
    row = []
    for k in range(size):
        if i == 0:
            row.append(k+1)
        else:
            if k == 0:
                row.append(i+1)
            else:
                row.append((i+1)*table[0][k])
    table.append(row)
html = '<html><body><table>'
for row in table:
    html += '<tr>'
    for value in row:
        html += '<td><a href=http://{}.ru>{}</a></td>'.format(value, value)
    html += '</tr>'
html += '</table></body></html>'
print(html)