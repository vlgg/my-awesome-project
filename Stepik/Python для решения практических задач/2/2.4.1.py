import urllib.request
import zipfile
import xlrd
import os

ved = {}
urllib.request.urlretrieve('https://stepik.org/media/attachments/lesson/245299/rogaikopyta.zip', 'salary.zip')
with zipfile.ZipFile('salary.zip', 'r') as zip_ref:
    zip_ref.extractall('salary_all')
for filename in os.listdir('salary_all'):
    wb = xlrd.open_workbook('salary_all//'+filename)
    sheet = wb.sheet_by_index(0)
    vals = [sheet.row_values(rownum) for rownum in range(sheet.nrows)]
    for i in vals[1:]:
        if i[0] == 'ФИО':
            if i[1] not in ved:
                ved[i[1]] = int(i[3])
            else:
                ved[i[1]] = int(ved[i[1]] + i[3])
for i in sorted(ved.keys()):
    print(i, ved[i], sep=" ")