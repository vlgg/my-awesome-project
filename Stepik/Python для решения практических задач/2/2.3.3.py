import xlrd

wb = xlrd.open_workbook('trekking3.xlsx')
sheet = wb.sheet_by_index(1)
vals = [sheet.row_values(rownum) for rownum in range(sheet.nrows)]
d = {}
energy = {"ccal": 0, "B": 0, "G": 0, "U": 0}

for i in vals[1:]:
    if i[0] not in d:
        d[i[0]] = {}
        if i[1] not in d[i[0]]:
            d[i[0]].update({i[1]: i[2]/100})
        else:
            d[i[0]].update({i[1]: d[i[0]][i[1]] + i[2] / 100})
    else:
        if i[1] not in d[i[0]]:
            d[i[0]].update({i[1]: i[2] / 100})
        else:
            d[i[0]].update({i[1]: d[i[0]][i[1]] + i[2] / 100})
sheet = wb.sheet_by_index(0)
vals = [sheet.row_values(rownum) for rownum in range(sheet.nrows)]
for i in d:
    for l in d[i]:
        for k in vals[1:]:
            if l == k[0]:
                if k[1]:
                    energy["ccal"] = energy["ccal"] + d[i][k[0]] * k[1]
                if k[2]:
                    energy["B"] = energy["B"] + d[i][k[0]] * k[2]
                if k[3]:
                    energy["G"] = energy["G"] + d[i][k[0]] * k[3]
                if k[4]:
                    energy["U"] = energy["U"] + d[i][k[0]] * k[4]
    for i in energy:
        energy[i] = int(energy[i])
    print(*energy.values(), sep=" ")
    energy = {"ccal": 0, "B": 0, "G": 0, "U": 0}
