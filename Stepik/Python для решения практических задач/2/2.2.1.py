import xlrd
import statistics

wb = xlrd.open_workbook('salaries.xlsx')
sheet = wb.sheet_by_index(0)
vals = [sheet.row_values(rownum) for rownum in range(sheet.nrows)]
name = ""
value = 0
for i in vals[1:]:
    lst = i[1:]
    lst.sort()
    if statistics.median(lst) > value:
        value = statistics.median(lst)
        name = i[0]
print(name, end=" ")
name = ""
value = 0
l = len(vals[0])
for i in range(l-1):
    avg = 0
    for k in vals[1:]:
        avg += k[i+1]
    avg = avg/(l-1)
    if avg > value:
        name = vals[0][i+1]
        value = avg
print(name)