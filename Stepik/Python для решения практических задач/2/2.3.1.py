import xlrd
import operator

wb = xlrd.open_workbook('trekking1.xlsx')
sheet = wb.sheet_by_index(0)
vals = [sheet.row_values(rownum) for rownum in range(sheet.nrows)]
d = {}
for i in vals[1:]:
    d[i[0]] = i[1]
sorted_x = [v[0] for v in sorted(d.items(), key=lambda kv: (-kv[1], kv[0]))]
for i in sorted_x:
    print(i)
