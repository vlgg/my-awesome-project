import urllib.request
import xml.etree.ElementTree as ET

urllib.request.urlretrieve('https://stepik.org/media/attachments/lesson/245678/map1.osm', 'map321.osm')
root = ET.parse('map321.osm').getroot()
nodes = 0
nodet = 0
for i in root.getchildren():
    if i.tag == 'node':
        nodes += 1
        for k in i:
            if k.tag == 'tag':
                nodet += 1
                break
print("%s %s" % (nodet, nodes-nodet))
